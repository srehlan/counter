import React, { PureComponent } from "react";
import "./App.css";
import Counters from "./components/Counters";
import TotalCounter from "./components/TotalCounter";

class App extends PureComponent {
  constructor() {
    super();
    this.state = {
      counters: [
        { id: 1, value: 0 },
        { id: 2, value: 2 },
        { id: 3, value: 3 },
        { id: 4, value: 0 },
      ],
    };
  }

  // componentDidMount() {
  //   console.log("app did mount");
  // }

  // componentDidUpdate() {
  //   console.log("app did update");
  // }
  // componentWillUnmount() {
  //   console.log("app did unmount");
  // }

  getTotalValue() {
    let totalValue = 0;
    this.state.counters.forEach((counter) => {
      totalValue += counter.value;
    });
    return totalValue;
  }

  incrementHandler = (id) => {
    const counters = [...this.state.counters];
    const index = counters.findIndex((counter) => counter.id === id);
    counters[index] = { ...counters[index] };
    counters[index].value += 1;
    this.setState({ counters: counters });
  };

  decrementHandler = (id) => {
    const counters = [...this.state.counters];
    const index = counters.findIndex((counter) => counter.id === id);
    counters[index] = { ...counters[index] };
    counters[index].value -= 1;
    this.setState({ counters: counters });
  };

  deleteHandler = (id) => {
    let counters = this.state.counters.filter((counter) => counter.id !== id);
    this.setState({ counters: counters });
  };

  render() {
    console.log("app render");

    return (
      <>
        <TotalCounter totalValue={this.getTotalValue()}></TotalCounter>
        <main className="container">
          <Counters
            onDelete={this.deleteHandler}
            onDecrement={this.decrementHandler}
            onIncrement={this.incrementHandler}
            counters={this.state.counters}
          ></Counters>
        </main>
      </>
    );
  }
}

export default App;
