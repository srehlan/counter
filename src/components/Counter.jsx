import React, { PureComponent } from "react";

class Counter extends PureComponent {
  render() {
    const { onIncrement, onDecrement, onDelete, counter } = this.props;
    return (
      <div>
        <span style={{ fontSize: 15 }} className="badge bg-secondary sm">
          {counter.value}
        </span>
        <button
          onClick={() => onIncrement(counter.id)}
          className="btn btn-primary sm m-2"
        >
          Increment
        </button>
        <button
          onClick={() => onDecrement(counter.id)}
          className="btn btn-secondary sm m-2"
        >
          Decrement
        </button>
        <button
          onClick={() => onDelete(counter.id)}
          className="btn btn-danger sm m-2"
        >
          Delete
        </button>
      </div>
    );
  }
}

export default Counter;
