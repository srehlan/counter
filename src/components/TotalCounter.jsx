import React, { PureComponent } from "react";

class TotalCounter extends PureComponent {
  render() {
    console.log("TotalCounter constructor");

    return (
      <div>
        <span style={{ fontSize: 25 }} className="badge bg-primary sm">
          {this.props.totalValue}
        </span>
      </div>
    );
  }
}

export default TotalCounter;
