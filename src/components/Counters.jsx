import React, { PureComponent } from "react";
import Counter from "./Counter";

class Counters extends PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    console.log("Counters constructor");

    return (
      <div>
        {this.props.counters.map((counter) => {
          return (
            <Counter
              key={counter.id}
              counter={counter}
              onDecrement={this.props.onDecrement}
              onIncrement={this.props.onIncrement}
              onDelete={this.props.onDelete}
            ></Counter>
          );
        })}
      </div>
    );
  }
}
export default Counters;
